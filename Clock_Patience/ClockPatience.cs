using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock_Patience
{
    internal class ClockPatience
    {
         static void Main(string[] args)
        {
            ClockPatience.Play();
        }
        static void Play()
        {
            while (true)
            {
                string[] deck = new string[52];
                int i = 0;
                while (true)
                {
                    string line = Console.ReadLine();
                    if (line == "#")
                    {
                        break;
                    }
                    string[] cards = line.Split(' ');
                    for (int j = 0; j < cards.Length; j++)
                    {
                        deck[i++] = cards[j];
                    }
                }
                if (deck[0] == null)
                {
                    break;
                }
                string[][] piles = new string[13][];
                for (int j = 0; j < 13; j++)
                {
                    piles[j] = new string[4];
                    for (int k = 0; k < 4; k++)
                    {
                        piles[j][k] = deck[j * 4 + k];
                    }
                }
                string currentCard = piles[12][3];
                int exposedCards = 1;
                while (true)
                {
                    try
                    {
                        int pileIndex = (currentCard[0] - 'A' + 1) % 13;
                        if (piles[pileIndex][3] == null)
                        {
                            break;
                        }
                        currentCard = piles[pileIndex][3];
                        piles[pileIndex][3] = null;
                        exposedCards++;
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                Console.WriteLine("{0:00},{1}", exposedCards, currentCard);
            }
        }

    }
}

